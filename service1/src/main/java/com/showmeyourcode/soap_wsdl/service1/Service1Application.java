package com.showmeyourcode.soap_wsdl.service1;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Service1Application {

	public static void main(String[] args) {
		log.info("Starting service1...");
		SpringApplication.run(Service1Application.class, args);
	}

}
