package com.showmeyourcode.soap_wsdl.service1.constant;

public class WebServiceConstant {
    public static final String NAMESPACE_URI = "showmeyourcode.com/soap-wsdl/service1";
    public static final String LOCATION_URI = "/ws";
    public static final String LOCATION_URI_MAPPINGS = "/ws/*";
    public static final String WSDL_DEFINITION_NAME = "service1-countries";
}
