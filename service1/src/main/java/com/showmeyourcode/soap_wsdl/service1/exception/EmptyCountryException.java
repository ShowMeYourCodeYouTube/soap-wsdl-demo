package com.showmeyourcode.soap_wsdl.service1.exception;

public class EmptyCountryException extends Exception {

    public EmptyCountryException(String s) {
        super(s);
    }
}
