package com.showmeyourcode.soap_wsdl.service1.endpoint;

import com.showmeyourcode.soap_wsdl.service1.generated.GetCountryRequest;
import com.showmeyourcode.soap_wsdl.service1.generated.GetCountryResponse;
import com.showmeyourcode.soap_wsdl.service1.constant.WebServiceConstant;
import com.showmeyourcode.soap_wsdl.service1.exception.EmptyCountryException;
import com.showmeyourcode.soap_wsdl.service1.repository.CountryRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Slf4j
@Endpoint
public class CountryEndpoint {

    private final CountryRepository countryRepository;

    public CountryEndpoint(CountryRepository countryRepository) {
        this.countryRepository = countryRepository;
    }

    @PayloadRoot(namespace = WebServiceConstant.NAMESPACE_URI, localPart = "getCountryRequest")
    @ResponsePayload
    public GetCountryResponse getCountry(@RequestPayload GetCountryRequest request) throws EmptyCountryException {
        log.info("getCountry request...");
        GetCountryResponse response = new GetCountryResponse();
        response.setCountry(countryRepository.findCountry(request.getName()));

        return response;
    }
}
