package com.showmeyourcode.soap_wsdl.service1.config;

import com.showmeyourcode.soap_wsdl.service1.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static com.showmeyourcode.soap_wsdl.service1.constant.WebServiceConstant.LOCATION_URI;
import static com.showmeyourcode.soap_wsdl.service1.constant.WebServiceConstant.WSDL_DEFINITION_NAME;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;


class WebServiceConfigTests extends IntegrationTestBase {

    @Test
    void shouldFetchWsdlDefinitionForCountries() throws Exception {
        String wsdlUri = "http://localhost:" + serverPort + "/" + LOCATION_URI + "/" + WSDL_DEFINITION_NAME + ".wsdl";
        RequestEntity<String> requestEntity = new RequestEntity<>(HttpMethod.GET, new URI(wsdlUri));


        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }
}
