package com.showmeyourcode.soap_wsdl.main.rest;

import com.showmeyourcode.soap_wsdl.client.generated.GetCountryResponse;
import com.showmeyourcode.soap_wsdl.client.generated.GetEmployeeOperationResponse;
import com.showmeyourcode.soap_wsdl.main.client.CountryClient;
import com.showmeyourcode.soap_wsdl.main.client.EmployeeClient;
import com.showmeyourcode.soap_wsdl.main.config.EndpointConstant;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(EndpointConstant.CONTROLLER_PREFIX)
public class ServicesController {

    private final CountryClient countryClient;
    private final EmployeeClient employeeClient;

    @GetMapping(EndpointConstant.CONTROLLER_SERVICE1_PREFIX)
    public GetCountryResponse callService1(){
        log.info("Trying to call the service1...");
        GetCountryResponse response = countryClient.getCountry("Spain");
        log.info("Received a response from the service1 - {}", response.getCountry());
        return response;
    }

    @GetMapping(EndpointConstant.CONTROLLER_SERVICE2_PREFIX)
    public GetEmployeeOperationResponse callService2(){
        log.info("Trying to call the service2...");
        GetEmployeeOperationResponse response = employeeClient.getEmployee(1);
        log.info("Received a response from the service2 - {}", response.getEmployeeResponse());
        return response;
    }
}
