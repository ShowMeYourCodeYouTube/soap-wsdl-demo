package com.showmeyourcode.soap_wsdl.main.client;

import com.showmeyourcode.soap_wsdl.client.generated.GetEmployeeOperation;
import com.showmeyourcode.soap_wsdl.client.generated.GetEmployeeOperationResponse;
import jakarta.xml.bind.JAXBElement;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import javax.xml.namespace.QName;

@Slf4j
public class EmployeeClient extends WebServiceGatewaySupport {

    /**
     * An example how to send a request to service2 - getEmployee.
     * @param employeeId - employee ID
     * @return employee object
     */
    public GetEmployeeOperationResponse getEmployee(int employeeId) {
        log.info("Get an employee from service2: {} / {}", employeeId, getWebServiceTemplate().getDefaultUri());
        GetEmployeeOperation request = new GetEmployeeOperation();
        request.setId(employeeId);

        // Unmarshal when @XMLRootElement is missing
        // Unmarshalling in JAXB is the process of converting XML content into a Java object.
        //
        // The XMLRootElement is missing which causes a following error:
        //
        // Request processing failed; nested exception is org.springframework.oxm.MarshallingFailureException:
        // JAXB marshalling exception; nested exception is javax.xml.bind.MarshalException with linked exception:
        // com.sun.istack.SAXException2: unable to marshal type "com.showmeyourcode.soap_wsdl.client.generated.GetEmployee"
        // as an element because it is missing an @XmlRootElement annotation
        //
        // Ref: https://codenotfound.com/jaxb-marshal-element-missing-xmlrootelement-annotation.html
        JAXBElement<GetEmployeeOperation> jaxbElement = new JAXBElement<>(
                new QName(
                        "http://ws.service2.soap_wsdl.showmeyourcode.com/",
                "GetEmployeeOperation"
                ),
                GetEmployeeOperation.class,
                request
        );

        return ((JAXBElement<GetEmployeeOperationResponse>) getWebServiceTemplate().marshalSendAndReceive(jaxbElement)).getValue();
    }
}
