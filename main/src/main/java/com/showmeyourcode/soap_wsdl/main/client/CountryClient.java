package com.showmeyourcode.soap_wsdl.main.client;

import com.showmeyourcode.soap_wsdl.client.generated.GetCountryRequest;
import com.showmeyourcode.soap_wsdl.client.generated.GetCountryResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

@Slf4j
public class CountryClient extends WebServiceGatewaySupport {

    /**
     * An example how to send a request to service1 - getCountry.
     * @param country - name of a country to get
     * @return country object
     */
    public GetCountryResponse getCountry(String country) {
        log.info("Get country from service1: {} / {}", country, getWebServiceTemplate().getDefaultUri());
        GetCountryRequest request = new GetCountryRequest();
        request.setName(country);

        return (GetCountryResponse) getWebServiceTemplate().marshalSendAndReceive(request);
    }
}
