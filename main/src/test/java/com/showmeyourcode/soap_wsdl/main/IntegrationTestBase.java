package com.showmeyourcode.soap_wsdl.main;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.StreamUtils;

import java.nio.charset.Charset;

import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;

@Slf4j
@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTestBase {

    protected static MockServer service1MockServer;
    protected static MockServer service2MockServer;

    @LocalServerPort
    protected int serverPort;

    @Autowired
    protected TestRestTemplate restTemplate;

    @Autowired
    private ResourceLoader resourceLoader;

    @BeforeAll
    static void setup() {
        log.info("Starting WireMock services...");
        if(IntegrationTestBase.service1MockServer == null){
            service1MockServer = new MockServer(options().port(8100));
        }
        if(IntegrationTestBase.service2MockServer == null){
            service2MockServer = new MockServer(options().port(8200));
        }

        service1MockServer.start();
        service2MockServer.start();
    }

    @AfterAll
    static void tearDown(){
        log.info("Stopping WireMock services...");
        service1MockServer.stop();
        service2MockServer.stop();
    }

    @SneakyThrows
    protected String loadResourceAsString(String path)  {
        return StreamUtils.copyToString(
                loadResource(path).getInputStream(),
                Charset.defaultCharset()
        ).trim();
    }


    @BeforeEach
    protected void beforeEach(){
        service1MockServer.clearStubs();
        service2MockServer.clearStubs();
    }

    private Resource loadResource(String path) {
        return resourceLoader.getResource("classpath:"+path);
    }
}
