# SOAP/WSDL demo

| Branch |                                                                                      Pipeline                                                                                      |                                                                                   Code coverage                                                                                    |                                      Test report                                      |
|:------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:-------------------------------------------------------------------------------------:|
| master | [![pipeline status](https://gitlab.com/ShowMeYourCodeYouTube/soap-wsdl-demo/badges/master/pipeline.svg)](https://gitlab.com/ShowMeYourCodeYouTube/soap-wsdl-demo/-/commits/master) | [![coverage report](https://gitlab.com/ShowMeYourCodeYouTube/soap-wsdl-demo/badges/master/coverage.svg)](https://gitlab.com/ShowMeYourCodeYouTube/soap-wsdl-demo/-/commits/master) | [link](https://showmeyourcodeyoutube.gitlab.io/soap-wsdl-demo/test-report/index.html) |

---

The **SOAP (Simple Object Access Protocol)** is a messaging protocol which facilitates connectivity in heterogeneous systems and is used to exchange structured information in Web services. Messages (requests and responses) are XML documents over HTTP. The XML contract is defined by the *WSDL (Web Services Description Language)*. It provides a set of rules to define the messages, bindings, operations, and location of the service.

**WSDL (Web Services Definition Language)** is a contract definition of the available services. It is a specification of input/output messages, and how to invoke the web service. It is language neutral and is defined in XML.

![SOAP1](./docs/soap-protocol1.png)

Reference: https://www.researchgate.net/figure/SOAP-protocol-SOAP-is-the-master-leader-in-communications-Its-main-purpose-is-to-send_fig1_327054573

![SOAP2](./docs/soap-protocol2.jpg)

Reference: https://thecustomizewindows.com/2014/03/simple-object-access-protocol-soap/

- The XML used in SOAP can become extremely complex. For this reason, it is best to use SOAP with a framework like JAX-WS or Spring.
- **JAXB** stands for Java Architecture for XML Binding. It provides mechanism to marshal (write) java objects into XML and unmarshal (read) XML into an object. *JAXB is an API used by JAX-WS.*
- **Java API for XML Web Services (JAX-WS)** is a standardized API for creating and consuming SOAP (Simple Object Access Protocol) web services.
- **Web service** - Service delivered over the web.
- **Apache CXF** is an open source services framework. CXF helps you build and develop services using frontend programming APIs, like JAX-WS and JAX-RS. These services can speak a variety of protocols such as SOAP, XML/HTTP, RESTful HTTP, or CORBA and work over a variety of transports such as HTTP, JMS or JBI. CXF has good integration with Spring.

## Getting Started

### Technology

- JDK
- SOAP
- Maven
- Spring Boot
- JAXB
- JAX-WS
- CXF
- Wiremock

### Project structure

- **service1**
  - The first web service built with the approach Top-Down.
  - In a **Top-Down (Contract-First) Approach**, a WSDL document is created, and the necessary Java classes are generated from the WSDL.
  - It uses *Spring Web Services (Spring-WS) and *@Endpoint annotation*.*.
- **service2**
  - The second web service built with the approach Bottom-Up.
  - In a **Bottom-Up (Contract-Last) Approach**, the Java classes are written, and the WSDL is generated from the Java classes. Writing a WSDL file can be quite difficult depending on how complex your web service is. This makes the bottom-up approach an easier option. On the other hand, since your WSDL is generated from the Java classes, any change in code might cause a change in the WSDL. This is not the case for the top-down approach.
  - It uses *Apache CXF* and *@WebService annotation*.
- **main**
  - The main module which calls service1 and service2. It exposes an HTTP endpoint and you call it 
- **coverage-report**
  - The module aggregates all test reports.

### First steps

1. Run Maven command: ``clean install`` which will fetch all dependencies and build all modules.
2. (Optional) If you are using Intellij and generated classes cannot be found, mark the jaxb directory as "Generated sources". ![IntelliJ setup](./docs/mark-directory-as-generated-sources.png)
3. Enable annotations processing required by Lombok in your IDE.
4. Run all modules.
5. Verify WSDLs:
    - service1: http://localhost:8100/ws/service1-countries.wsdl
    - service2: http://localhost:8200/services/service2-employee?wsdl
6. (Optional) Check what is a default mapping for web services:
    - http://localhost:8200/actuator/mappings
    ```text
    {
      "mappings": [
        "/services/*"
      ],
      "name": "CXFServlet",
      "className": "org.apache.cxf.transport.servlet.CXFServlet"
    }
    ```
    - http://localhost:8200/services
    ![Available SOAP services](./docs/service2-soap-services.png)
7. Verify SOAP services.
   1. Use SoapUI to verify `service1` and `service2`.
       - You can import the SoapUI project from `soap-wsdl-demo-soapui-project.xml` ![SoapUI service1](./docs/soap-ui.png)
   2. Or call the *main* module using HTTP request.
    ```text
    GET http://localhost:8000/api/v1/services/service1
    ```

## WSDL document overview

[WSDL service1](./service1/src/main/resources/service1.xsd)

Tags:
- xmlns:xs
  - It indicates that the elements and data types used in the schema come from the given namespace.
- xmlns:tns
  - It lets you refer to the namespace later in the schema. For example, if you declare a named type and then want to also declare an element of that type:
  ```
  <complexType name="someType">
    <!-- ... -->
  </complexType>
  
  <element name="someElement" type="tns:someType" />
  ```
- targetNamespace
  - The value of targetNamespace is simply a unique identifier, typically a company may use their URL followed by something descriptive to qualify it. In principle the namespace has no meaning, but some companies have used the URL where the schema is stored as the targetNamespace, and so some XML parsers will use this as a hint path for the schema:
  ```
  targetNamespace="http://www.microsoft.com/CommonTypes.xsd"
  ```
  However, the following would be equally valid:
  ```
  targetNamespace="my-common-types"
  ```

### References

- https://www.liquid-technologies.com/xml-schema-tutorial/xsd-namespaces
- https://stackoverflow.com/questions/17295588/xmlntns-and-targetnamespace
- https://www.w3schools.com/xml/schema_schema.asp

## Reference Documentation

For further reference, please consider the following sections:

* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.5.5/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.5.5/maven-plugin/reference/html/#build-image)
* [Spring Web Services](https://docs.spring.io/spring-boot/docs/2.5.5/reference/htmlsingle/#boot-features-webservices)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.5.5/reference/htmlsingle/#boot-features-developing-web-applications)
* [Apache CXF](https://github.com/codecentric/cxf-spring-boot-starter)

### Guides

The following guides illustrate how to use some features concretely:

- https://spring.io/guides/gs/producing-web-service/
- https://spring.io/guides/gs/consuming-web-service/
- https://www.baeldung.com/spring-boot-soap-web-service
- https://www.baeldung.com/jax-ws
- https://www.baeldung.com/spring-soap-web-service
