package com.showmeyourcode.soap_wsdl.service2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;

import static com.showmeyourcode.soap_wsdl.service2.constant.WebServiceConstant.DEFAULT_CXF_URI;
import static com.showmeyourcode.soap_wsdl.service2.constant.WebServiceConstant.WSDL_DEFINITION_NAME;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public abstract class IntegrationTestBase {

    @LocalServerPort
    protected int serverPort;

    @Autowired
    protected TestRestTemplate restTemplate;

    protected String getEmployeeServiceUri(){
        return "http://localhost:" + serverPort + DEFAULT_CXF_URI+"/" + WSDL_DEFINITION_NAME;
    }
}
