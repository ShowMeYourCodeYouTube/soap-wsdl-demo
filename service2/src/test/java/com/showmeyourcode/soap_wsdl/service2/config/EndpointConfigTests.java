package com.showmeyourcode.soap_wsdl.service2.config;

import com.showmeyourcode.soap_wsdl.service2.IntegrationTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;

import java.net.URI;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EndpointConfigTests extends IntegrationTestBase {

    @Test
    void shouldFetchWsdlDefinitionForEmployees() throws Exception {
        String wsdlUri = getEmployeeServiceUri() + "?wsdl";
        RequestEntity<String> requestEntity = new RequestEntity<>(HttpMethod.GET, new URI(wsdlUri));


        ResponseEntity<String> responseEntity = restTemplate.exchange(requestEntity, String.class);

        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertNotNull(responseEntity.getBody());
    }
}
