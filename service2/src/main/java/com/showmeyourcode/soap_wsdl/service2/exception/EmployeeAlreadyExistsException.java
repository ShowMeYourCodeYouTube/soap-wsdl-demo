package com.showmeyourcode.soap_wsdl.service2.exception;

public class EmployeeAlreadyExistsException extends Exception {
    public EmployeeAlreadyExistsException() {
        super();
    }
}
