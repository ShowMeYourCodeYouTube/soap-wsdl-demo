package com.showmeyourcode.soap_wsdl.service2;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Service2Application {

    public static void main(String[] args) {
        log.info("Starting service2...");
        SpringApplication.run(Service2Application.class, args);
    }

}
