package com.showmeyourcode.soap_wsdl.service2.repository;

import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeAlreadyExistsException;
import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeDoesNotExistException;
import com.showmeyourcode.soap_wsdl.service2.model.EmployeeEntity;
import jakarta.annotation.PostConstruct;
import org.springframework.stereotype.Repository;


import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

@Repository
public class EmployeeRepository {

    private static final Map<Integer, EmployeeEntity> employees = new HashMap<>();
    private static final AtomicInteger idx=new AtomicInteger(1);

    @PostConstruct
    private void initData(){
        int employee1Id=idx.getAndIncrement();
        employees.put(employee1Id, EmployeeEntity.builder().id(employee1Id).firstName("Johnny").build());
        int employee2Id=idx.getAndIncrement();
        employees.put(employee2Id, EmployeeEntity.builder().id(employee2Id).firstName("David").build());
        int employee3Id=idx.getAndIncrement();
        employees.put(employee3Id, EmployeeEntity.builder().id(employee3Id).firstName("Matthew").build());
    }

    public EmployeeEntity getEmployee(int id){
        return employees.get(id);
    }

    public EmployeeEntity updateEmployee(int id, String name) throws EmployeeDoesNotExistException {
        EmployeeEntity currentEmployee = getEmployee(id);
        if(Objects.isNull(currentEmployee)){
            throw new EmployeeDoesNotExistException();
        }

        currentEmployee.setFirstName(name);
        employees.put(id, currentEmployee);

        return currentEmployee;
    }

    public boolean deleteEmployee(int id){
        employees.remove(id);
        return true;
    }

    public EmployeeEntity addEmployee(int id, String name) throws EmployeeAlreadyExistsException {
        if(employees.containsKey(id)){
            throw new EmployeeAlreadyExistsException();
        }
        EmployeeEntity newEmployee = EmployeeEntity.builder().firstName(name).id(id).build();
        employees.put(id, newEmployee);
        return newEmployee;
    }
}
