package com.showmeyourcode.soap_wsdl.service2.constant;

public class WebServiceConstant {
    public static final String WSDL_DEFINITION_NAME = "service2-employee";
    public static final String DEFAULT_CXF_URI="/services";
}
