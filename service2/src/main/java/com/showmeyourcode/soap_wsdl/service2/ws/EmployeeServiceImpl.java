package com.showmeyourcode.soap_wsdl.service2.ws;

import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeAlreadyExistsException;
import com.showmeyourcode.soap_wsdl.service2.exception.EmployeeDoesNotExistException;
import com.showmeyourcode.soap_wsdl.service2.model.Employee;
import com.showmeyourcode.soap_wsdl.service2.model.EmployeeEntity;
import com.showmeyourcode.soap_wsdl.service2.repository.EmployeeRepository;
import org.springframework.stereotype.Component;

@Component
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public Employee getEmployee(int id) {
        EmployeeEntity entity = employeeRepository.getEmployee(id);
        return mapEmployee(entity);
    }


    public Employee updateEmployee(int id, String name) throws EmployeeDoesNotExistException {
        EmployeeEntity entity = employeeRepository.updateEmployee(id, name);
        return mapEmployee(entity);
    }

    public boolean deleteEmployee(int id) {
        return employeeRepository.deleteEmployee(id);
    }

    public Employee addEmployee(int id, String name) throws EmployeeAlreadyExistsException {
        EmployeeEntity entity = employeeRepository.addEmployee(id, name);
        return mapEmployee(entity);
    }

    private Employee mapEmployee(EmployeeEntity entity) {
        return Employee.builder()
                .id(entity.getId())
                .firstName(entity.getFirstName())
                .build();
    }
}
